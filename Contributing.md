#Contributing
Si vous souhaitez apporter votre contribution à ce projet, il vous est demandé de discuter, dans un premier temps, des modifications que vous souhaitez effectuer avec l'équipe de TodoList via les issues.

Nous avons défini un code de conduite, toute demande ne respectant pas ses principes sera automatiquement refusée. 

###Code de conduite

Cee projet et toute personne y participant se doit de respecter notre code de conduite défini ci-dessous.

####Notre engagement

Dans l'intérêt de favoriser un environnement ouvert et accueillant, nous, en tant que contributeurs et responsables, nous engageons à faire de la participation à notre projet et à notre communauté une expérience sans harcèlement pour tout le monde, indépendamment de l'âge, de la taille du corps, du handicap, de l'ethnicité, de l'identité de genre et de l'expression, niveau d'expérience, nationalité, apparence personnelle, race, religion, ou identité et orientation sexuelles.

####Nos standards
Exemple de comportement participant à créer un environnement de travail agréable et positif:
* Etre respectueux des différents points de vue
* Accepter les critiques constructives
* Se concentrer sur ce qui est bénéfique à la communauté
* Montrer de l'empathie pour les autres membres de la communauté
  
Exemples de comportements inacceptables de la part des participants:
* Tenir un langage injurieux ou à caractère sexuel
* Dire des insultes, troller, se moquer, faire des attaques personnelles, parler politique
* Harceler publiquement ou en privé
* Publier des informations privées telles que l'adresse d'une autre personne sans son consentement
* Autre type de conduite qui pourrait être à raison considéré comme étant inapproprié en milieu professionnel
  
####Notre responsabilité

Les mainteneurs du projet sont responsables de clarifier les standards du comportement attendu de la part des participants, et prendront des mesures correctives adaptées, temporaires ou permanentes, en réponse à tout manquement à ceux-ci.

Le mainteneurs du projet ont la responsabilité de supprimer, éditer ou rejeter des commentaires, commits, du code, des issues et autres contributions qui ne sont pas alignées à ce code de conduite, ou de bannir temporairement ou de manière permanente tout contributeur ayant fait preuve de comportement inadéquat, menaçant, aggressif ou harcelant.

####Portée du code de conduite

Ce code de conduite s'applique à la fois dans le repository du projet, mais également dans les espaces publics où un membre de l'équipe est amené à représenter le projet. Il peut s'agir par exemple d'évènements tels que des publications diffusées sur des réseaux sociaux, ou un évènement en ligne ou en présentiel.

####Application

Les actes abusifs, frauduleux et autres comportements inacceptables doivent être reportées à l'équipe à l'adresse contact@todolist.fr . Chaque plainte sera étudiée et l'équipe jugera de sa pertinence. L'équipe du projet est obligée de garder secret toute remarque qui lui sera faite par égard à la personne dénonçant l'abus.

Les mainteneurs du projet qui ne suivent pas ce code de conduite peuvent faire face à des sanctions temporaires ou permanentes.

#### Attribution

Ce code de conduite est adapté des <a href="https://www.contributor-covenant.org/">[Cotributor Covenant]</a>, version 2.0, accessibles au lien <a href="https://www.contributor-covenant.org/">https://www.contributor-covenant.org/</a>.

##Comment puis-je contribuer ?

Pull Request Process
1. Forkez le projet
2. Créez une branche sur votre local
3. Pushez et créez une merge request
4. Assurez-vous que le fichier composer ne comporte pas d'ajout de nouvelles dépendances, ou de mise à jour sur celles qui existent
5. Mettez à jour le README.md lorsque cela le nécessite
6. Demandez à un développeur de l'équipe de merger votre pull request une fois que vous êtes assuré de sa conformité

Plus d'informations:
<a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork">https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html</a>


####Remonter des bugs

Avant de reporter un bug, merci de vous assurer que quelqu'un ne s'en est pas déjà chargé en lisant les issues en ligne. Vous pourrez ainsi apporter votre contribution sur cette issue et apporter des précisions sur le bug constaté.

Lorsque vous rapportez un dysfonctionnement, merci de rapporter **le plus de détails possibles**. Il vous faudra remplir le template suivant:

> ####Prérequis
> ####Description
> ####Etapes à reproduire
> 1.
> 2.
> 3.
> #####Comportement attendu:
> #####Comportement actuel:
> ####Version
> ####Autres informations

####Rapport de bug correct

Les bugs sont reportés via les issues. Il vous faut donc créer une issue sur ce repository et reporter les informations suivantes en complétant le template ci-dessus.

Expliquez le problème en incluant des détails pour aider les mainteneurs du projet à résoudre le problème:
* Utilisez un titre clair et explicite pour l'issue
* Décrivez les étapes exactes pour reproduire le problème avec le plus de détails possible. Par exemple, quelles ont été les commandes exactes utilisées dans le terminal, où quelles actions ont été exécutées avec l'application.
* Donnez des exemples concrets pour illustrer les étapes ci-dessus. Incluez des liens vers des fichiers ou projets github, copiez des snippets que vous avez utilisé dans vos exemples.
* Décrivez le comportement observé après avoir suivi ces étapes
* Décrivez le comportement que vous attendiez et pourquoi
* Incluez des screenshots ou gif pour démontrer clairement le problème
* Si vous reportez un crash de l'application, merci de poster la stack trace permettant de remonter à la source du problème
* Si le problème n'est pas déclenché par une action de votre part, décrivez vos actions lors de sa survenue
 
Fournissez le contexte en répondant à ces questions:
* Le problème est-il intervenu récemment ?
* Le problème est-il intervenu après une mise à jour ?
* Pouvez-vous le reproduire avec une ancienne version de l'application ?
* Pouvez-vous reproduire le problème de manière récurrente ? Si non, fournissez des détails sur sa survenue
  
Fournissez des détails sur votre environnement:
* Votre projet est-il bien à jour ?
* Quel est votre OS ? Sa version ?
* Avez-vous lancé l'application sur une VM? Si oui, avec quel logiciel, et quels OS hôte et VM?
* Avez-vous modifié les fichiers de configuration? Si oui, lesquels?
  
####Proposition d'amélioration correcte

Merci de suivre cette ligne de conduite pour aider les développeurs et la communauté à comprendre votre suggestion et trouver celles en lien avec cette dernière.

Les propositions d'amélioration sont proposées via les issues. Il vous faut donc créer une issue sur ce repository et reporter les informations suivantes en complétant le template ci-dessus.

Avant de soumettre une proposition, vérifiez que la suggestion n'a pas déjà été proposée en lisant les issues existantes sur ce repository. Si c'est le cas, commentez-la au lieu d'en écrire une nouvelle.

Lors de la création d'une issue, fournissez les infos suivantes:
* Utilisez un titre clair et explicite
* Décrivez les étapes exactes de votre suggestion avec le plus de détails possible.
* Donnez des exemples concrets pour illustrer les étapes ci-dessus.
* Décrivez le comportement actuel observé et celui que vous proposez après avoir suivi ces étapes
* Incluez des screenshots ou gif pour démontrer clairement votre suggestion
* Proposez des exemples d'application où cette amélioration existe
* Spécifiez le nom et la version de votre OS
  

####Pull request process
Le process décrit ci-après a pour objectif:
* De maintenir la qualité de l'application
* D'unifier la compréhension du code à toute la communauté
* D'améliorer le code de tout participant en maintenant de bonnes pratiques
* De faciliter le traitement des pull request aux mainteneurs du projet
  
Merci de respecter ces étapes pour voir votre contribution prise en compte par les mainteneurs du projet:
1. Suivez toutes les instructions définies ci-dessus
2. Suivez le guide de qualité  défini ci-après
3. Après avoir soumis votre pull request, assurez-vous que la pipeline passe

Les indications ci-dessus sont indispensables si vous souhaitez voir votre pull request prise en compte par l'équipe de développement. Les reviewers pourront vous demander des informations complémentaires, des tests ou d'effectuer quelques changements avant que votre pull request soit acceptée.

##Guide de qualité

####Messages de commit

* Ecrivez vos messages en anglais
* Utilisez le présent
* Référencez l'issue ou la pull request concernée en début de message (#56 Add feature)
* Limitez la première ligne à 70 caractères
* Si les modifications ne concernent que la documentation, incluez le flag \[DOC] en début de message
  
####Qualité de code

Ce projet est surveillé par plusieurs outils d'analyse de qualité de code, installés via composer :
* Php code sniffer (configuration personnalisée)
* Php cs fixer
* Php stan (avec extensions symfony et doctrine)

Commandes pour exécuter chacun de ces outils. Assurez-vous d'avoir installé les dépendances :
```php
  php vendor/squizlabs/php_codesniffer/bin/phpcs src 
  php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix  
  php vendor/phpstan/phpstan/phpstan analyse src
```

Le projet utilise Grumphp, qui se charge de lancer les outils cités ci-dessus à chaque commit. En cas d'erreur dans le style relevé par un de ces de ces auditeurs de code, le commit échouera jusqu'a la correction de l'erreur de style.

Vous pouvez le lancer manuellement via la commande:

```php
  php ./vendor/phpro/grumphp/bin/grumphp
```

Le style est donc systématiquement contrôlé avant chaque acceptation de PR.

Le projet est également surveillé par un compte Codacy. A chaque pull request, une pipeline est lancée par Gitlab, qui démarre les tests et l'analyse Codacy
