<?php

declare(strict_types=1);

namespace App\Tests\Doctrine\Listener;

use App\Doctrine\Listener\PrePersistListener;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Persistence\ObjectManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;

/**
 * @internal
 */
class PrePersistListenerTest extends TestCase
{
    public function getSecurity()
    {
        return $this->createMock(Security::class);
    }

    public function getManager()
    {
        return $this->createMock(ObjectManager::class);
    }

    public function testUserPersistedDoesntSetCurrentUser()
    {
        $security = $this->getSecurity();

        $lifeCycleEventArgs = $this->getMockBuilder(LifecycleEventArgs::class)
            ->setConstructorArgs([new User(), $this->getManager()])
            ->getMock()
        ;

        $lifeCycleEventArgs->expects(static::once())->method('getObject');
        $security->expects(static::never())->method('getUser');
        $lifeCycleEventArgs->expects(static::never())->method('getObjectManager');

        $listener = new PrePersistListener($security);
        $listener->prePersist($lifeCycleEventArgs);
    }

    public function testTaskPersistedSetCurrentUser()
    {
        $security      = $this->getSecurity();
        $objectManager = $this->getManager();
        $task          = $this->createMock(Task::class);
        $user          = new User();

        $lifeCycleEventArgs = $this->getMockBuilder(LifecycleEventArgs::class)
            ->setConstructorArgs([$task, $objectManager])
            ->getMock()
        ;

        $security->expects(static::once())->method('getUser')->willReturn($user);
        $lifeCycleEventArgs->expects(static::once())->method('getObject')->willReturn($task);
        $lifeCycleEventArgs->expects(static::once())->method('getObjectManager')->willReturn($objectManager);
        $task->expects(static::once())->method('setUser')->with($user);
        $objectManager->expects(static::once())->method('flush');

        $listener = new PrePersistListener($security);
        $listener->prePersist($lifeCycleEventArgs);
    }
}
