<?php

declare(strict_types=1);

namespace Tests\Controller;

use App\Controller\SecurityController;
use App\DataFixtures\Tests\UserFixtures;
use App\Tests\TestController;

/**
 * @internal
 */
class SecurityControllerTest extends TestController
{
    public const LOGIN  = '/login';
    public const LOGOUT = '/logout';

    public function testLoginPageNotLogged()
    {
        $this->assertSuccessful(self::LOGIN);
        $this->assertSelectorTextContains('h1', 'Se connecter');
        $this->assertSelectorNotExists('.alert-danger');
    }

    public function testLoginPageAlreadyLogged()
    {
        $this->assertRedirected(self::LOGIN, 1);
    }

    public function testInvalidLogins()
    {
        $client  = self::createClient();
        $crawler = $client->request('GET', self::LOGIN);
        $form    = $crawler->selectButton('Se connecter')->form([
            'username' => 'invalid@credential.fr',
            'password' => 'dummyPass',
        ]);
        $client->submit($form);

        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }

    public function testValidLogins()
    {
        $this->loadFixtures([UserFixtures::class]);
        $client = self::createClient();

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');

        $client->request('POST', self::LOGIN, [
            'username'    => 'anonymous',
            'password'    => 'coucou',
            '_csrf_token' => $csrfToken->getValue(),
        ]);

        $this->assertResponseRedirects('/');
    }

    public function testLogout()
    {
        $client = self::createClient();
        $client->enableProfiler();
        $this->loadFixtures([UserFixtures::class]);
        self::bootKernel();

        $session = $this->logIn($client, self::$container, 1);

        $client->request('GET', 'logout');

        static::assertNull($session->get('_security_main'));
        static::assertNull((new SecurityController())->logoutCheck());
    }
}
