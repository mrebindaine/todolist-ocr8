<?php

declare(strict_types=1);

namespace Tests\Controller;

use _HumbugBox09702017065e\Nette\NotSupportedException;
use App\DataFixtures\Tests\UserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\TestController;

/**
 * @internal
 */
class UserControllerTest extends TestController
{
    public function testUserListPageNotLogged()
    {
        $this->assertRedirected('/utilisateurs');
    }

    public function testUserListPageLoggedAsAdmin()
    {
        $this->assertSuccessful('/utilisateurs', 2);
        $this->assertSelectorTextContains('h2', 'Liste des utilisateurs');
    }

    public function testUserListPageLoggedAsUser()
    {
        $this->assertForbidden('/utilisateurs', 3);
    }

    public function testCreateUserPageNotLogged()
    {
        $this->assertSuccessful('/creer-utilisateur');
    }

    public function testCreateUserPageLogged()
    {
        $this->assertRedirected('/creer-utilisateur', 1);
    }

    public function testCreateUser()
    {
        $client  = self::createClient();
        $crawler = $client->request('GET', '/creer-utilisateur');

        $client->enableProfiler();
        $form = $crawler->selectButton('Envoyer')->form([
            'user[username]'         => 'coucou',
            'user[password][first]'  => 'motdepasse',
            'user[password][second]' => 'motdepasse',
            'user[email]'            => 'testemail@test.fr',
            'user[role]'             => User::ROLE_USER,
        ]);
        $client->submit($form);

        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testCreateInvalidUser()
    {
        $client  = self::createClient();
        $crawler = $client->request('GET', '/creer-utilisateur');

        $client->enableProfiler();
        $form = $crawler->selectButton('Envoyer')->form([
            'user[username]'         => '',
            'user[password][first]'  => 'motdepasse',
            'user[password][second]' => 'motdepasseerrone',
            'user[email]'            => 'testemailfalse',
            'user[role]'             => User::ROLE_USER,
        ]);
        $client->submit($form);

        $this->assertSelectorExists('.invalid-feedback');
    }

    public function testEditUserPageNotLogged()
    {
        $this->assertRedirected('/utilisateur/1/edit');
    }

    public function testEditUserPageLoggedAsAdmin()
    {
        $this->assertSuccessful('/utilisateur/1/edit', 2);
    }

    public function testEditUserPageLoggedAsUser()
    {
        $this->assertForbidden('/utilisateur/1/edit', 3);
    }

    public function testEditUserFormIsFilledWhenLogged()
    {
        $this->loadFixtures([UserFixtures::class]);
        /** @var User $user */
        $user = $this->getContainer()->get(UserRepository::class)->find(1);

        $client = self::createClient();
        $this->logIn($client, self::$container, 2);
        $crawler = $client->request('GET', '/utilisateur/1/edit');

        $form = $crawler->selectButton('Envoyer')->form();
        static::assertSame($form->get('user[username]')->getValue(), $user->getUsername());
        static::assertSame($form->get('user[email]')->getValue(), $user->getEmail());
        static::assertSame($form->get('user[role]')->getValue(), $user->getRole());
    }

    public function testEditUserValid()
    {
        $this->loadFixtures([UserFixtures::class]);

        $client = self::createClient();
        $this->logIn($client, self::$container, 2);
        $crawler = $client->request('GET', '/utilisateur/1/edit');

        $form = $crawler->selectButton('Envoyer')->form([
            'user[username]'         => 'coucou',
            'user[password][first]'  => 'motdepasse',
            'user[password][second]' => 'motdepasse',
            'user[email]'            => 'testemail@test.fr',
            'user[role]'             => User::ROLE_USER,
        ]);

        $client->submit($form);
        $this->assertResponseRedirects('/utilisateurs');

        $client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testDeleteUserAsAdmin()
    {
        self::bootKernel();
        $this->loadFixtures([UserFixtures::class]);

        $userToDelete = self::$container->get(UserRepository::class)->find(3);
        static::assertNotNull($userToDelete);

        $client = self::createClient();
        $this->logIn($client, self::$container, 2);
        $client->request('GET', '/utilisateur/3/delete');

        $this->assertResponseRedirects('/profil');
        $client->followRedirect();

        $this->assertSelectorExists('.alert-success');
        $userToDelete = self::$container->get(UserRepository::class)->find(3);
        static::assertNull($userToDelete);
    }

    /**
     * @throws NotSupportedException
     */
    public function testDeleteUserAsUser()
    {
        self::bootKernel();
        $this->loadFixtures([UserFixtures::class]);

        $client = self::createClient();
        $this->logIn($client, self::$container, 1);
        $client->request('GET', '/utilisateur/3/delete');

        $userToDelete = self::$container->get(UserRepository::class)->find(3);
        static::assertNotNull($userToDelete);
        $this->assertSelectorNotExists('.alert-success');
    }
}
