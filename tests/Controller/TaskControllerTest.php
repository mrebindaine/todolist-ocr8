<?php

declare(strict_types=1);

namespace Tests\Controller;

use App\DataFixtures\Tests\TaskFixtures;
use App\DataFixtures\Tests\UserFixtures;
use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Tests\TestController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 */
class TaskControllerTest extends TestController
{
    public function loadTasks()
    {
        $this->loadFixtures([UserFixtures::class, TaskFixtures::class]);
    }

    public function testFinishedTaskListPagesNotLogged()
    {
        $this->assertRedirected('/taches-finies');
    }

    public function testFinishedTaskListPagesLogged()
    {
        $this->assertSuccessful('/taches-finies', 3);
    }

    public function testCreateTaskPageLogged()
    {
        $this->assertSuccessful('/creer-tache', 3);
        $this->assertSelectorTextContains('h2', 'Créer une tâche');
    }

    public function testCreateTaskPageNotLogged()
    {
        $this->assertRedirected('/creer-tache');
    }

    public function testCreateValidTaskAsUser()
    {
        $client = self::createClient();
        $this->logIn($client, self::$container, 3);
        $crawler = $client->request('GET', '/creer-tache');

        $form = $crawler->selectButton('Envoyer')->form([
            'task[title]'   => 'Aller faire les courses',
            'task[content]' => 'Jai plus rien a manger...',
        ]);
        $client->submit($form);

        $this->assertResponseRedirects('/profil');
        $client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testCreateInvalidTaskAsUser()
    {
        $client = self::createClient();
        $this->logIn($client, self::$container, 3);
        $crawler = $client->request('GET', '/creer-tache');

        $form = $crawler->selectButton('Envoyer')->form([
            'task[title]'   => '',
            'task[content]' => '',
        ]);
        $client->submit($form);

        $this->assertSelectorExists('.invalid-feedback');
    }

    public function testTaskEditingFormIsFilled()
    {
        $this->loadTasks();
        /** @var Task $task */
        $task = $this->getContainer()->get(TaskRepository::class)->find(1);

        $client = self::createClient();
        $this->logIn($client, self::$container, 1);
        $crawler = $client->request('GET', '/tache/1/edit');

        $form = $crawler->selectButton('Envoyer')->form();
        static::assertSame($form->get('task[title]')->getValue(), $task->getTitle());
        static::assertSame($form->get('task[content]')->getValue(), $task->getContent());
    }

    public function testEditTaskNotLogged()
    {
        $this->loadTasks();
        $this->assertRedirected('/tache/1/edit');
    }

    public function testEditTaskAsAuthor()
    {
        $this->loadTasks();

        $client = self::createClient();
        $this->logIn($client, self::$container, 1);
        $crawler = $client->request('GET', '/tache/1/edit');

        $form = $crawler->selectButton('Envoyer')->form([
            'task[title]'   => 'Aller faire les courses',
            'task[content]' => 'Jai plus rien a manger...',
        ]);

        $client->submit($form);
        $this->assertResponseRedirects();

        $client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testEditTaskNotAsAuthor()
    {
        $this->loadTasks();

        $client = self::createClient();
        $this->logIn($client, self::$container, 2);
        $client->request('GET', '/tache/1/edit');

        static::assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testToggleTaskAsAuthor()
    {
        $this->loadTasks();
        /** @var Task $task */
        $task = $this->getContainer()->get(TaskRepository::class)->find(1);
        static::assertFalse($task->isDone());

        $client = self::createClient();
        $this->logIn($client, self::$container, 1);
        $client->request('GET', '/tache/1/toggle');

        $this->assertResponseRedirects();
        $client->followRedirect();

        $this->assertSelectorExists('.alert-success');
    }

    public function testToggleTaskNotAsAuthor()
    {
        $this->loadTasks();
        /** @var Task $task */
        $task = $this->getContainer()->get(TaskRepository::class)->find(1);
        static::assertFalse($task->isDone());

        $client = self::createClient();
        $this->logIn($client, self::$container, 2);
        $client->request('GET', '/tache/1/toggle');

        static::assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testDeleteTaskAsAuthor()
    {
        self::bootKernel();
        $this->loadTasks();
        /** @var Task $task */
        $task = self::$container->get(TaskRepository::class)->find(1);
        static::assertNotNull($task);

        $client = self::createClient();
        $this->logIn($client, self::$container, 1);
        $client->request('GET', '/tache/1/delete');

        $this->assertResponseRedirects('/profil');
        $client->followRedirect();

        $this->assertSelectorExists('.alert-success');
        $task = self::$container->get(TaskRepository::class)->find(1);
        static::assertNull($task);
    }

    public function testDeleteTaskNotAsAuthor()
    {
        $this->loadTasks();
        $client = self::createClient();
        $this->logIn($client, self::$container, 3);
        $client->request('GET', '/tache/1/delete');
        static::assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function testDeleteAnonymousTaskAsAdmin(){
        self::bootKernel();
        $this->loadTasks();
        /** @var Task $task */
        $task = self::$container->get(TaskRepository::class)->find(1);
        static::assertNotNull($task);

        $client = self::createClient();
        $this->logIn($client, self::$container, 2);
        $client->request('GET', '/tache/1/delete');

        $this->assertResponseRedirects('/profil');
        $client->followRedirect();

        $this->assertSelectorExists('.alert-success');
        $task = self::$container->get(TaskRepository::class)->find(1);
        static::assertNull($task);
    }
}
