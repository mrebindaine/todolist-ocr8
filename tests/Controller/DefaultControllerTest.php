<?php

declare(strict_types=1);

namespace Tests\Controller;

use App\Tests\TestController;

/**
 * @internal
 */
class DefaultControllerTest extends TestController
{
    public function testIndex()
    {
        $this->assertSuccessful('/', 1);
    }

    public function testProfilePageLogged()
    {
        $this->assertSuccessful('/profil', 1);
    }

    public function testProfilePageNotLogged()
    {
        $this->assertRedirected('/profil');
    }
}
