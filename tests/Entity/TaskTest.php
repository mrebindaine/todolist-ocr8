<?php

declare(strict_types=1);

namespace Tests\Entity;

use App\Entity\Task;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class TaskTest extends KernelTestCase
{
    public function getEntity()
    {
        $task = new Task();
        $task->setUser(new User())->setContent('Je dois acheter des pâtes et du papier toilette !')->setTitle('Faire les courses');
        return $task;
    }

    public function assertHasError(Task $task, int $errorsExpected) {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($task);
        $message = [];

        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $message[] = $error . ' => ' . $error->getMessage();
        }

        $this->assertCount($errorsExpected, $errors, implode(',', $message));
    }

    public function testValidEntity() {
        $this->assertHasError($this->getEntity(), 0);
    }

    public function testTitle()
    {
        $title = 'Titre de test';
        $task = $this->getEntity()->setTitle($title);
        $this->assertSame($title, $task->getTitle());
    }

    public function testInvalidTitle()
    {
        $this->assertHasError($this->getEntity()->setTitle(''), 1);
    }

    public function testContent()
    {
        $content = 'Contenu de test';
        $task = $this->getEntity()->setContent($content);
        $this->assertSame($content, $task->getContent());
    }

    public function testInvalidContent()
    {
        $this->assertHasError($this->getEntity()->setContent(''), 1);
    }

    public function testToggle()
    {
        $task = $this->getEntity();

        static::assertFalse($task->isDone());
        $task->toggle();
        static::assertTrue($task->isDone());
    }

    public function testGetCreatedAt()
    {
        static::assertNotNull($this->getEntity()->getCreatedAt());
    }

    // test user is set
}
