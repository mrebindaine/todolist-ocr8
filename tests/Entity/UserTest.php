<?php

declare(strict_types=1);

namespace Tests\Entity;

use App\DataFixtures\Tests\UserFixtures;
use App\Entity\Task;
use App\Entity\User;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class UserTest extends WebTestCase
{
    use FixturesTrait;

    public function getUser() : User
    {
        $user     = new User();
        $user->setUsername('Massya')->setPassword('P4sSw0rD')->setRole(User::ROLE_USER)->setEmail('test@test.fr');
        return $user;
    }

    public function assertHasError(User $user, int $errorsExpected) {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($user);
        $message = [];

        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $message[] = $error . ' => ' . $error->getMessage();
        }

        $this->assertCount($errorsExpected, $errors, implode(',', $message));
    }

    public function testValidUser()
    {
        $this->assertHasError($this->getUser(), 0);
    }

    public function testUsername()
    {
        $username = '';
        $user = $this->getUser()->setUsername($username);
        $this->assertSame($username, $user->getUsername());
    }

    public function testInvalidUsername()
    {
        $this->assertHasError($this->getUser()->setUsername(''), 1);
    }


    public function testUsernameAlreadyRegistered() {
        self::bootKernel();
        $this->loadFixtures([UserFixtures::class]);
        $this->assertHasError($this->getUser()->setUsername('anonymous'), 1);
    }

    public function testPassword()
    {
        $password = 'Password de test';
        $user = $this->getUser()->setPassword($password);
        $this->assertSame($password, $user->getPassword());
    }

    public function testInvalidPassword()
    {
        $this->assertHasError($this->getUser()->setPassword(''), 1);
    }

    public function testSaltIsNull()
    {
        static::assertNull($this->getUser()->getSalt());
    }

    public function testEraseCredentialsReturnsNull()
    {
        static::assertNull($this->getUser()->eraseCredentials());
    }

    public function testEmail()
    {
        $email = 'test@test.fr';
        $user = $this->getUser()->setEmail($email);
        $this->assertSame($email, $user->getEmail());
    }

    public function testInvalidEmail()
    {
        $this->assertHasError($this->getUser()->setEmail('Ceci est un titre, pas un email !'), 1);
        $this->assertHasError($this->getUser()->setEmail(''), 1);
    }

    public function testEmailAlreadyRegistered() {
        self::bootKernel();
        $this->loadFixtures([UserFixtures::class]);
        $this->assertHasError($this->getUser()->setEmail('anonymous@test.fr'), 1);
    }

    public function testRole()
    {
        $role = User::ROLE_ADMIN;
        $user = $this->getUser()->setRole($role);
        $this->assertSame($role, $user->getRole());
    }

    public function testInvalidRole()
    {
        $this->assertHasError($this->getUser()->setRole('Ce role existe pas !'), 1);
        $this->assertHasError($this->getUser()->setRole(''), 2);
    }

    public function testAddingTask()
    {
        $user = new User();
        $task = new Task();
        $user->addTask($task);
        static::assertCount(1, $user->getTasks());
    }

    public function testRemovingTask()
    {
        $user = new User();
        $task = new Task();
        $user->addTask($task);
        $user->removeTask($task);
        static::assertCount(0, $user->getTasks());
    }

    public function testGetRoles()
    {
        $user = new User();

        static::assertIsArray($user->getRoles());
        $this->assertSame([User::ROLE_USER], $user->getRoles());

        $user->setRole(User::ROLE_ADMIN);
        $this->assertSame([User::ROLE_ADMIN], $user->getRoles());
    }
}
