<?php

declare(strict_types=1);

namespace Tests\Form;

use App\Entity\Task;
use App\Form\TaskType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Test\FormIntegrationTestCase;
use Symfony\Component\Validator\Validation;

/**
 * @internal
 */
class TaskFormTest extends FormIntegrationTestCase
{
    public function testSubmitValidTask()
    {
        $title   = 'Titre de test !';
        $content = 'Test de contenu !';

        $formData = [
            'title'   => $title,
            'content' => $content,
        ];

        $taskToCompare = new Task();

        $form = $this->factory->create(TaskType::class, $taskToCompare);

        $task = (new Task())
            ->setTitle($title)
            ->setContent($content)
        ;

        $form->submit($formData);

        static::assertTrue($form->isSynchronized());
        static::assertSame($task->getTitle(), $taskToCompare->getTitle());
        static::assertSame($task->getContent(), $taskToCompare->getContent());

        $view     = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            static::assertArrayHasKey($key, $children);
        }
    }

    protected function getExtensions()
    {
        $validator = Validation::createValidator();

        return [
            new ValidatorExtension($validator),
        ];
    }
}
