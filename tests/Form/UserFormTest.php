<?php

declare(strict_types=1);

namespace Tests\Form;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Test\FormIntegrationTestCase;
use Symfony\Component\Validator\Validation;

/**
 * @internal
 */
class UserFormTest extends FormIntegrationTestCase
{
    public function testSubmitValidUser()
    {
        $formData = [
            'username' => 'Morgane',
            'email'    => 'mrebindaine@hotmail.com',
            'role'     => User::ROLE_ADMIN,
        ];

        $userToCompare = new User();

        $form = $this->factory->create(UserType::class, $userToCompare);

        $user = (new User())
            ->setUsername('Morgane')
            ->setEmail('mrebindaine@hotmail.com')
            ->setRole(User::ROLE_ADMIN)
        ;

        $form->submit($formData);

        static::assertTrue($form->isSynchronized());
        static::assertEquals($user, $userToCompare);

        $view     = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            static::assertArrayHasKey($key, $children);
        }
    }

    protected function getExtensions()
    {
        $validator = Validation::createValidator();

        return [
            new ValidatorExtension($validator),
        ];
    }
}
