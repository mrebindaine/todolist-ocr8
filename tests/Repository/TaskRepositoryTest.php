<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\DataFixtures\Tests\TaskFixtures;
use App\DataFixtures\Tests\UserFixtures;
use App\Repository\TaskRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 */
class TaskRepositoryTest extends WebTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([UserFixtures::class, TaskFixtures::class]);

        $tasks = self::$container->get(TaskRepository::class)->count([]);
        static::assertSame(10, $tasks);
    }

    public function testAnonymousTaskCall()
    {
        self::bootKernel();
        $this->loadFixtures([UserFixtures::class, TaskFixtures::class]);

        $tasks = self::$container->get(TaskRepository::class)->findAnonymous();

        foreach ($tasks as $task) {
            static::assertSame('anonymous', $task->getUser()->getUsername());
        }

    }
}
