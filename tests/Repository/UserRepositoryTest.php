<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\DataFixtures\Tests\UserFixtures;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 */
class UserRepositoryTest extends WebTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([UserFixtures::class]);

        $users = self::$container->get(UserRepository::class)->count([]);
        static::assertSame(11, $users);
    }
}
