#TodoList

TodoList est une application permettant de gérer l'ensemble de ses tâches sans effort.

Ce projet est le 8ème et dernier du parcours Développeur Symfony par OpenClassroom, et a pour objectif l'amélioration de code pré-existant, la mise en place de tests et le profiling de celui-ci.

##Mise en place

####Pré-requis
L'installation de ce projet nécessite composer, Mysql, Php +7

####Installation
* Clonez le projet dans un répertoire sur votre local
* Mettez à jour le fichier .env
* Lancez "composer install"
* Démarrez le serveur via la commande "symfony server:start"
* Lancez les fixtures "php bin/console d:f:l'
* Rendez vous sur localhost:8000 pour tester l'application

####Tests
Tous les tests sont réalisés à l'aide de PhpUnit.
Pour les lancer, utilisez la commande php bin/phpunit.

##Descriptif
TodoList est une application permettant de gérer sa propre liste de tâches à faire !
Elle donne accès à une interface intuitive après s'être inscrit sur la plateforme.

Lors de l'inscription, deux rôles sont disponibles:
* Administrateur
* Utilisateur

Les utilisateurs peuvent créer des tâches, les modifier ou bien les supprimer.

Les administrateurs ont la capacité de gérer les utilisateurs (édition et suppression), en plus d'avoir les mêmes droits que les simples utilisateurs. Ils peuvent également supprimer les tâches "archivées" non liées à un utilisateur.

## Contributions

Consultez le [fichier descriptif du process de contribution](Contributing.md).

**Author**: Rebindaine Morgane