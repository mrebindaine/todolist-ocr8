<?php

declare(strict_types=1);

namespace App\DataFixtures\Tests;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const ANONYMOUS_USER = 'anonymous-user';

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $anonymous = new User();
        $anonymous->setUsername('anonymous')
            // password = coucou
            ->setPassword('$argon2i$v=19$m=65536,t=4,p=1$d3o1SndQLk1oaEV5Y2hVZQ$QCEOn/Paq+KXN38v2nPGcNza1xSVbec3KhesieeOXrg')
            ->setEmail('anonymous@test.fr')
            ->setRole(User::ROLE_USER)
        ;
        $manager->persist($anonymous);

        $admin = new User();
        $admin->setUsername('admin')
            // password = coucou
            ->setPassword('$argon2i$v=19$m=65536,t=4,p=1$d3o1SndQLk1oaEV5Y2hVZQ$QCEOn/Paq+KXN38v2nPGcNza1xSVbec3KhesieeOXrg')
            ->setEmail('admin@test.fr')
            ->setRole(User::ROLE_ADMIN)
        ;
        $manager->persist($admin);

        for ($i = 0; $i < 9; ++$i) {
            $user = (new User())->setUsername('user' . $i)
                ->setPassword('$argon2i$v=19$m=65536,t=4,p=1$d3o1SndQLk1oaEV5Y2hVZQ$QCEOn/Paq+KXN38v2nPGcNza1xSVbec3KhesieeOXrg')
                ->setEmail('user' . $i . '@test.fr')
                ->setRole(User::ROLE_USER)
            ;
            $manager->persist($user);
        }

        $manager->flush();

        $this->addReference(self::ANONYMOUS_USER, $anonymous);
    }
}
