<?php

declare(strict_types=1);

namespace App\Doctrine\Listener;

use App\Entity\Task;
use Doctrine\Common\EventArgs;
use Symfony\Component\Security\Core\Security;

class PrePersistListener
{
    /** @var Security */
    private $security;

    /**
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @param EventArgs $args
     */
    public function prePersist(EventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Task) {
            return;
        }

        $currentUser = $this->security->getUser();
        $entity->setUser($currentUser);

        $manager = $args->getObjectManager();
        $manager->flush();
    }
}
