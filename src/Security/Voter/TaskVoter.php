<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\Task;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TaskVoter extends Voter
{
    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['edit', 'delete'])
            && $subject instanceof Task;
    }

    /**
     * @param string         $attribute
     * @param Task           $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case 'delete':
                return $subject->getUser()->getId() === $user->getId() || ($subject->hasAnonymousAuthor() && $user->isAdmin());
            case 'edit':
                return $subject->getUser()->getId() === $user->getId();
        }

        return false;
    }
}
