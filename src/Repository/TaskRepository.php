<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

class TaskRepository extends ServiceEntityRepository
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * @param ManagerRegistry $registry
     * @param UserRepository  $userRepository
     */
    public function __construct(ManagerRegistry $registry, UserRepository $userRepository)
    {
        parent::__construct($registry, Task::class);
        $this->userRepository = $userRepository;
    }

    /**
     * @return ArrayCollection|null
     */
    public function findAnonymous()
    {
        return $this->createQueryBuilder('t')
            ->where('t.user = :user')
            ->setParameter('user', $this->userRepository->findBy(['username' => 'anonymous']))
            ->getQuery()
            ->getResult()
        ;
    }
}
