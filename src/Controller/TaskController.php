<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class TaskController extends AbstractController
{
    /**
     * @Route("/taches-finies", name="done_task_list")
     *
     * @param TaskRepository $taskRepository
     * @param Security       $security
     *
     * @return Response
     */
    public function listDoneTasks(TaskRepository $taskRepository, Security $security)
    {
        return $this->render('task/list.html.twig', [
            'tasks'      => $taskRepository->findBy(['isDone' => 1, 'user' => $security->getUser()]),
            'statusTask' => 'terminées',
        ]);
    }

    /**
     * @Route("/taches-a-faire", name="todo_task_list")
     *
     * @param TaskRepository $taskRepository
     * @param Security       $security
     *
     * @return Response
     */
    public function listTodoTasks(TaskRepository $taskRepository, Security $security)
    {
        return $this->render('task/list.html.twig', [
            'tasks'      => $taskRepository->findBy(['isDone' => 0, 'user' => $security->getUser()]),
            'statusTask' => 'en cours',
        ]);
    }

    /**
     * @Route("/creer-tache", name="task_create")
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($task);
            $em->flush();

            $this->addFlash('success', 'La tâche a été bien été ajoutée.');

            return $this->redirectToRoute('profile');
        }

        return $this->render('task/form.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Créer une tâche',
        ]);
    }

    /**
     * @Route("/tache/{id}/edit", name="task_edit")
     *
     * @param Task    $task
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function edit(Task $task, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $task);

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'La tâche a bien été modifiée.');

            return  $this->redirectToTaskRoute($task);
        }

        return $this->render('task/form.html.twig', [
            'form'  => $form->createView(),
            'title' => 'Modifier une tâche',
        ]);
    }

    /**
     * @Route("/tache/{id}/toggle", name="task_toggle")
     *
     * @param Task $task
     *
     * @return RedirectResponse
     */
    public function toggleTask(Task $task)
    {
        $this->denyAccessUnlessGranted('edit', $task);

        $task->toggle();
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', sprintf('La tâche %1$s a bien été marquée comme %2$s.', $task->getTitle(), $task->isDone() ? 'faite' : 'étant en cours'));

        return $this->redirectToTaskRoute($task);
    }

    /**
     * @Route("/tache/{id}/delete", name="task_delete")
     *
     * @param Task                   $task
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse
     */
    public function deleteTask(Task $task, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('delete', $task);

        $em->remove($task);
        $em->flush();

        $this->addFlash('success', 'La tâche a bien été supprimée.');

        return $this->redirectToRoute('profile');
    }

    /**
     * @Route("/taches-anonymes", name="anonymous_tasks")
     *
     * @param TaskRepository $taskRepository
     *
     * @return Response
     */
    public function anonymousTask(TaskRepository $taskRepository)
    {
        return $this->render('task/list.html.twig', [
            'tasks'      => $taskRepository->findAnonymous(),
            'statusTask' => 'd\'anonymes',
        ]);
    }

    /**
     * @param Task $task
     *
     * @return RedirectResponse
     */
    public function redirectToTaskRoute(Task $task)
    {
        $route = $task->isDone() ? 'done_task_list' : 'todo_task_list';

        return  $this->redirectToRoute($route);
    }
}
