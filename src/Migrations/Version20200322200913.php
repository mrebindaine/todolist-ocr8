<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200322200913 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add user field in Task (and fill it with anonymous user), role field in User';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE task ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_527EDB25A76ED395 ON task (user_id)');
        $this->addSql('ALTER TABLE user ADD role VARCHAR(255) NOT NULL');
        $this->addSql('INSERT INTO user (username, password, email, role) VALUES (\'anonymous\', \'$argon2i$v=19$m=65536,t=4,p=1$YWlrQjBKWE01NWxYWnhmRQ$RwHhxQXUN6mRuEwb9dRVOWIEF/emaAo9EwKSGzS7LKw\', \'anonymous@dummy.fr\', \'ROLE_ADMIN\');');
        $this->addSql('
            UPDATE task t 
            SET t.user_id = (
                SELECT id FROM user WHERE username="anonymous"
            )
            WHERE t.user_id IS NULL
        ');
        $this->addSql('UPDATE user SET role = "ROLE_USER" WHERE role IS NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25A76ED395');
        $this->addSql('DROP INDEX IDX_527EDB25A76ED395 ON task');
        $this->addSql('
            UPDATE task t
            SET t.user_id = NULL
            WHERE t.user_id = (
                SELECT id FROM user WHERE username="anonymous"
            )
        ');
        $this->addSql('ALTER TABLE task DROP user_id');
        $this->addSql('ALTER TABLE user DROP role');
        $this->addSql('DELETE FROM user WHERE username = "anonymous"');
    }
}
