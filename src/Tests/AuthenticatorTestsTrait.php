<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

trait AuthenticatorTestsTrait
{
    public function logIn(KernelBrowser $client, ContainerInterface $container, int $idUserToLog): SessionInterface
    {
        /** @var User $user */
        $user    = $container->get(UserRepository::class)->find($idUserToLog);
        $session = $container->get('session');
        $token   = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $session;
    }
}
