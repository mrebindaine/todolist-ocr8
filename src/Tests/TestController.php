<?php

declare(strict_types=1);

namespace App\Tests;

use App\DataFixtures\Tests\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 * @covers
 */
class TestController extends WebTestCase
{
    use AuthenticatorTestsTrait;
    use FixturesTrait;

    public function get(string $route, $log = null)
    {
        $client = static::createClient();

        if ($log) {
            $this->loadFixtures([UserFixtures::class]);
            $this->logIn($client, self::$container, $log);
        }

        $client->request('GET', $route);

        return $client;
    }

    public function assertSuccessful(string $route, $log = null)
    {
        $this->get($route, $log);
        $this->assertResponseIsSuccessful();
    }

    public function assertForbidden(string $route, $log = null)
    {
        $client = $this->get($route, $log);

        static::assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }

    public function assertRedirected(string $route, $log = null)
    {
        $client = $this->get($route, $log);

        static::assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());
        $redirect = $log ? '/' : '/login';
        $this->assertResponseRedirects($redirect);
    }
}
